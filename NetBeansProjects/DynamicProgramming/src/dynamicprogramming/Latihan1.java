package dynamicprogramming;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Latihan1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String identitas = "Michael Kevin / XR6 / 30";
        tampilJudul(identitas);

        int n = tampilInput();

        BigInteger hasil = fibo(n);

        tampilHasil(n, hasil);
    }

    private static void tampilJudul(String identitas) {
        System.out.println("Identitas : " + identitas);

        System.out.println("==Hitung Fibonanci==");
        System.out.println("1,1,2,3,4,5,6, ...dst");
    }

    private static int tampilInput() {
        Scanner masuk = new Scanner(System.in);

        System.out.print("Bilangan ke-: ");
        int n = masuk.nextInt();

        return n;
    }

    private static BigInteger fibo(int n) {
        BigInteger[] hasil = new BigInteger[n];

        hasil[0] = BigInteger.ONE;
        hasil[1] = BigInteger.ONE;

        for (int i = 2; i < n; i++) {
            hasil[i] = hasil[i - 1].add(hasil[i - 2]);

        }
        return hasil[n - 1];
    }

    private static void tampilHasil(int n, BigInteger hasil) {
        System.out.println("Bilangan fibonanci ke-" + n + ":" + hasil);
    }
}
